package com.ihorbehen.serialization_deserialization;

import java.io.*;

class Droid implements Serializable {
    public String pubString = "Ab1";
    private int privInt = 100;
    transient int transInt = 200;
    transient static int transStatInt = 300;
    transient final int transFinalInt = 400;

    public static void main(String[] args) throws Exception{
        Droid input = new Droid();

        FileOutputStream fos = new FileOutputStream("abc.txt");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(input);

        FileInputStream fis = new FileInputStream("abc.txt");
        ObjectInputStream ois = new ObjectInputStream(fis);
        Droid output = (Droid)ois.readObject();
        System.out.println("pubString = " + output.pubString);
        System.out.println("privInt = " + output.privInt);
        System.out.println("transInt = " + output.transInt);
        System.out.println("transStatInt = " + output.transStatInt);
        System.out.println("transFinalInt  = " + output.transFinalInt );
    }
}
