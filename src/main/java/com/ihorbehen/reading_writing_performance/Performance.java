package com.ihorbehen.reading_writing_performance;

import java.io.*;

public class Performance {

    public static void main(String[] args) throws IOException {
        Performance performance = new Performance();
        performance.withoutBuffer();
        performance.withBuffer();
        System.out.println("###  2048 byte:");
        int bufferSize = 1 * 2048;
        performance.withCustomizableBuffer(bufferSize);
        System.out.println("###  1024 byte:");
        bufferSize = 1 * 1024;
        performance.withCustomizableBuffer(bufferSize);
        System.out.println("###  512 byte:");
        bufferSize = 1 * 512;
        performance.withCustomizableBuffer(bufferSize);
        System.out.println("###  128 byte:");
        bufferSize = 1 * 128;
        performance.withCustomizableBuffer(bufferSize);
        System.out.println("###  64 byte:");
        bufferSize = 1 * 64;
        performance.withCustomizableBuffer(bufferSize);
        System.out.println("###  32 byte:");
        bufferSize = 1 * 32;
        performance.withCustomizableBuffer(bufferSize);
        System.out.println("###  16 byte:");
        bufferSize = 1 * 16;
        performance.withCustomizableBuffer(bufferSize);
        System.out.println("###  8 byte:");
        bufferSize = 1 * 8;
        performance.withCustomizableBuffer(bufferSize);
        System.out.println("###  4 byte:");
        bufferSize = 1 * 4;
        performance.withCustomizableBuffer(bufferSize);
        System.out.println("###  2 byte:");
        bufferSize = 1 * 2;
        performance.withCustomizableBuffer(bufferSize);
    }

    /**
     * reading without buffer
     * @throws IOException
     */
    void withoutBuffer() throws IOException {
        System.out.println("File is reading without buffer....");
        InputStream inputStream = new FileInputStream("why140mb.pdf");
        int data = inputStream.read();
        long start = System.currentTimeMillis();
        int count = 0;
        while (data != -1) {
            data = inputStream.read();
            count++;
        }
        inputStream.close();
        System.out.println("count: " + count);
        System.out.println("Done!");
        long end = System.currentTimeMillis();
        float sec = (end - start) / 1000F;
        System.out.println(sec + " seconds");
        float min = (end - start) / (60 * 1000F);
        System.out.println(min + " minutes");
    }

    /**
     * reading with buffer
     * @throws IOException
     */
    void withBuffer() throws IOException {
        System.out.println("File is reading with buffer....");
        DataInputStream disB = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream("why140mb.pdf")));
        long start = System.currentTimeMillis();
        int count1 = 0;
        try {
            while (true) {
                disB.readByte();
                count1++;
            }
        } catch (EOFException e) {
        }
        disB.close();
        System.out.println("count: " + count1);
        System.out.println("Done");
        long end = System.currentTimeMillis();
        float sec = (end - start) / 1000F;
        System.out.println(sec + " seconds");
        float min = (end - start) / (60 * 1000F);
        System.out.println(min + " minutes");
    }

    /**
     * reading with customizable buffer
     * @param bufferSize
     * @throws IOException
     */
    void withCustomizableBuffer(int bufferSize) throws IOException {
        bufferSize = 1 * 1024 * 1024;
        System.out.println("File is reading with customizable buffer....");
        DataInputStream disB1 = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream("why140mb.pdf"), bufferSize));
        long start = System.currentTimeMillis();
        int count2 = 0;
        try {
            while (true) {
                 disB1.readByte();
                count2++;
            }
        } catch (EOFException e) {
        }
        disB1.close();
        System.out.println("count: " + count2);
        System.out.println("Done");
        long end = System.currentTimeMillis();
        float sec = (end - start) / 1000F;
        System.out.println(sec + " seconds");
        float min = (end - start) / (60 * 1000F);
        System.out.println(min + " minutes");
    }
}
