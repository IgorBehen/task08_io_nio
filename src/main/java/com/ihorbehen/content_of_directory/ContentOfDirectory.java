package com.ihorbehen.content_of_directory;

import java.io.File;
import java.io.IOException;

public class ContentOfDirectory {

    public static void main(String[] args) throws IOException {
        ContentOfDirectory contentOfDirectory = new ContentOfDirectory();
        contentOfDirectory.showContentDirectory();
    }

    private void showContentDirectory() {
        System.out.println("Printing directory...");
        File file = new File("src");
        if (file.exists()) {
            printDirectory(file, "");
        } else {
            System.out.println("No directory");
        }
        System.out.println("This is end of directory files.");
    }

    private void printDirectory(File file, String noStr) {
        System.out.println(noStr + "Directory: " + file.getName());
        noStr = noStr + " ";
        File[] fileNames = file.listFiles();
        for (File f : fileNames) {
            if (f.isDirectory()) {
                printDirectory(f, noStr);
            } else {
                System.out.println(noStr + "File: " + f.getName());
            }
        }

    }
}