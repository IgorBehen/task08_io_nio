package com.ihorbehen.pushback_inputstream;

import java.io.*;

public class pushBackInputStream {

    public static void main(String[] args) {
        String strExpression = "abc;*num**var/xyz@";
        byte bytes[] = strExpression.getBytes();
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        PushbackInputStream pis = new PushbackInputStream(bis);
        int ch;
        try {
            while( (ch = pis.read())!= -1) {
                if(ch == '*') {
                    if( (ch = pis.read()) == '*') {
                        System.out.print(" 'asterisk & asterisk' ");
                    }
                    else {
                        pis.unread(ch);
                        System.out.print("*");
                    }
                }
                else {
                    System.out.print((char)ch);
                }
            }
        }
        catch(IOException ioe) {
            System.out.println("Exception while reading" + ioe);
        }
    }
}
