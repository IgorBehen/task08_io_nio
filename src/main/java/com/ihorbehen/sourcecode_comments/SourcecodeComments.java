package com.ihorbehen.sourcecode_comments;

import java.io.*;
import java.nio.file.Files;

public class SourcecodeComments {

    public static void main(String[] args) throws IOException {

        String filePath = "src/main/java/com/ihorbehen/reading_writing_performance/Performance.java";
        byte[] bFile = Files.readAllBytes(new File(filePath).toPath());
        String strExpression = "abc;* /** *@throws IOException   */ xyz@  abc;* /**  *@param bufferSize */ xyz@";
        byte bytes[] = strExpression.getBytes();
        int ch;
        try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
             PushbackInputStream pis = new PushbackInputStream(bis);) {
            while ((ch = pis.read()) != -1) {
                if ((ch = pis.read()) == '/') {
                    System.out.print((char) ch);
                    if ((ch = pis.read()) == '*') {
                        System.out.print((char) ch);
                        if ((ch = pis.read()) == '*') {
                            System.out.print((char) ch);
                            if ((ch = pis.read()) == ' ') {
                                System.out.println("");
                            }
                            while ((ch = pis.read()) == ' ') {
                                System.out.print("");
                            }
                            System.out.print("  " + (char) ch);
                            while ((ch = pis.read()) != '*') {
                                System.out.print((char) ch);
                            }
                            System.out.print("\n  " + (char) ch);
                            if ((ch = pis.read()) == '/') {
                                System.out.print((char) ch);
                            } else {
                                pis.unread(ch);
                            }
                        }
                    } else {
                        pis.unread(ch);
                    }
                    System.out.println();
                } else {
                    pis.unread(ch);
                }
            }
        } catch (
                IOException ioe) {
            System.out.println("Exception while reading" + ioe);
        }
    }
}
